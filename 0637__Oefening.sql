USE ModernWays;
-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
-- we gebruiken hiervoor een subquery
insert into Personen (Voornaam, Familienaam)
select distinct Voornaam, Familienaam from Boeken;

-- de andere kolommen toevoegen
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);
   
-- een foreign key aanmaken zodat we het kunnen linken
-- we moeten dit eerst null maken zodat we er nog informatie in kunnen toevoegen
-- het geeft een foutmelding met NOT NULL omdat er dan niets in zit en dat werkt tegendraads
alter table Boeken add Personen_Id int null;

-- vul de juiste ID's in waar de naam overeen komt met de waarden uit personen
-- zo kennen we geen verkeerde ID's toe
update Boeken cross join Personen
set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and Boeken.Familienaam = Personen.Familienaam;

-- nu kunnen we het wel NOT NULL maken aangezien we de info erin hebben gestoken
-- zo word het dus een verplichte waarde
alter table Boeken change Personen_Id Personen_Id int not null;

-- de kolommen voornaam en familienaam uit Boeken zijn nu overbodig en dus deleten we ze
alter table Boeken 
drop column Voornaam,
drop column Familienaam;

-- en nu maken we de foreign key die eerder is gemaakt echt een foreign key als constraint
alter table Boeken add constraint fk_Boeken_Personen
foreign key(Personen_Id) references Personen(Id);