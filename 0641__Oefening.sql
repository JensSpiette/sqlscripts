DELIMITER $$
use aptunes $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `NumberOfGenres`(
out Aantal tinyint
)
BEGIN
Select distinct count(Naam)
into Aantal
from Genres;
END $$
DELIMITER ;