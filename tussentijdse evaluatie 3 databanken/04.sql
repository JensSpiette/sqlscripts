USE tussentijdseevaluatie1bdb;
CREATE VIEW BoekenInfo
as
Select Boeken.Titel, concat(Auteurs.Voornaam,' ',Auteurs.Familienaam) as "Naam auteur", Uitgeverijen.Naam as "Naam uitgeverijen" 
from Boeken
INNER JOIN Auteurs on Boeken.Auteurs_Id = Auteurs.Id
INNER JOIN Uitgeverijen on Boeken.Uitgeverijen_Id = Uitgeverijen.ID