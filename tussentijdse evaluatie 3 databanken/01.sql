USE tussentijdseevaluatie1bdb;
Select Boeken.Titel , concat(Leden.Voornaam,' ',Leden.Familienaam) as "Naam"
from Ontleningen
INNER JOIN Leden on Ontleningen.Leden_Id = Leden.Id
INNER JOIN Boeken on Ontleningen.Boeken_Id = Boeken.Id;