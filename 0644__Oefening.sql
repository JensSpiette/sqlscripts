USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumRelease`;

DELIMITER $$
USE `aptunes`$$
CREATE  PROCEDURE `MockAlbumRelease`()
BEGIN
	DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId INT DEFAULT 0; 
    DECLARE randomBandId INT DEFAULT 0;
    
    Select COUNT(*) Into numberOfAlbums From Albums;
    Select COUNT(*) Into numberOfBands From Bands;
    SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
    SET randomBandId = FLOOR(RAND() * numberOfBands) + 1; 
    IF (randomAlbumId, randomBandId) not in (select * from albumreleases)
    THEN
		INSERT INTO AlbumReleases(Albums_Id, Bands_Id)
        VALUES (randomAlbumId, randomBandId);
	END IF;
END$$

DELIMITER ;
    