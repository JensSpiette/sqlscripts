-- een voorbeeld met een gegroepeerde kolom
USE ModernWays;
SELECT AVG(Leeftijd)
FROM Honden
GROUP BY Geslacht
HAVING Geslacht = 'mannelijk';