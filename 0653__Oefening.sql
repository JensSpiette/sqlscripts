USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration2`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration2` (
IN album INT,
OUT totalDuration SMALLINT UNSIGNED)
SQL SECURITY invoker
BEGIN
	DECLARE songDuration TINYINT UNSIGNED DEFAULT 0;
    DECLARE ok BOOL DEFAULT false;
    DECLARE songDurationCursor cursor for select Lengte from Liedjes where Album_Id = album;
    DECLARE continue handler for not found set ok = true;
    set songDuration = 0;
    open songDurationCursor;
    fetchloop : loop
    fetch songDurationCursor into songDuration;
    if ok = true then
    leave fetchloop;
    end if;
    set totalDuration = totalDuration + songDuration;
    end loop;
    close songDurationCursor;
END$$

DELIMITER ;