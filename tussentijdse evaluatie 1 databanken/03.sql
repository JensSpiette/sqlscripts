USE tussentijdseevaluatie;
CREATE TABLE Personen(
Voornaam varchar(100) collate utf8mb4_0900_as_ci NOT NULL,
Familienaam varchar(100) collate utf8mb4_0900_as_ci NOT NULL,
GeboorteDatum date NOT NULL,
GSMNummer varchar(10) NOT NULL
);