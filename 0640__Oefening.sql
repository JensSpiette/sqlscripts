use aptunes
DELIMITER $$  
CREATE PROCEDURE `Getliedjes`(
in Zoekwoord VARCHAR(50)
)
BEGIN 
Select Titel From liedjes
Where Titel Like Concat('%',Zoekwoord,'%');
END $$
DELIMITER ;