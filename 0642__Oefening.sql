use aptunes
DELIMITER $$
CREATE PROCEDURE `CleanupOldMemberships`(
in enddate Date ,out num int)
BEGIN
start transaction;
select count(*) into num
from Lidmaatschappen
where lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < enddate;
SET SQL_SAFE_UPDATES = 0;
DELETE FROM Lidmaatschappen
WHERE lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < enddate;
SET SQL_SAFE_UPDATES = 1;
commit;
END$$
DELIMITER ;