Use ModernWays;
select Id from Studenten 
inner join Evaluaties on Studenten.Id = Evaluaties.Studenten_Id
group by Studenten_Id 
having avg(Cijfer) > (select avg(Cijfer) from Evaluaties);