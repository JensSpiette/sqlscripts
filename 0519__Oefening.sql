USE ModernWays;
Alter table liedjes ADD Column Genre varchar(20) charset utf8mb4;
SET SQL_SAFE_UPDATES = 0;
update liedjes
set Genre = 'Hard Rock'
where Artiest = 'Led Zeppelin';
update liedjes 
set Genre = 'Hard Rock'
where Artiest = 'Van Halen';
SET SQL_SAFE_UPDATES = 1;