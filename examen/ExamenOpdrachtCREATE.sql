DROP DATABASE IF EXISTS Examen;
CREATE DATABASE Examen;
USE Examen;
CREATE TABLE Merken(
Naam VARCHAR(100) NOT NULL,
Id int auto_increment Primary key);
-- maak hier de tabel Merken aan zodat volgende instructies zullen werken
-- een merknaam bevat maximum 100 karakters en is verplicht

INSERT INTO Merken (Naam)
VALUES
('IBM'),
('HP'),
('Dell'),
('Bull'),
('Sun'),
('Acer');
CREATE TABLE Categorieen(
Omschrijving VARCHAR(100) NOT NULL,
Id int auto_increment Primary Key);
-- maak hier de tabel Categorieen aan zodat volgende instructies zullen werken
-- een omschrijving bevat maximum 100 karakters en is verplicht

INSERT INTO Categorieen (Omschrijving)
VALUES
('pc'),
('printer'),
('server');
CREATE TABLE Processoren(
Omschrijving VARCHAR (100) NOT NULL,
Id int auto_increment Primary Key);
-- maak hier de tabel Processoren aan zodat volgende instructies zullen werken
-- een omschrijving bevat maximum 100 karakters en is verplicht

INSERT INTO Processoren (Omschrijving)
VALUES
('3 Ghz'),
('300 Mhz'),
('2,6 Ghz');
CREATE TABLE Subcategorieen(
Omschrijving VARCHAR(100) NOT NULL,
Id int auto_increment Primary Key,
Categorieen_Id INT,
CONSTRAINT fk_Subcategorieen_Categorieen
FOREIGN KEY (Categorieen_Id)
REFERENCES Categorieen(Id));

-- maak hier de tabel Subcategorieën aan zodat volgende instructies zullen werken
-- een omschrijving bevat maximum 100 karakters en is verplicht
-- een subcategorie verwijst verplicht naar een categorie
 
INSERT INTO Subcategorieen (Omschrijving,Categorieen_Id)
VALUES
('desktop',1),
('laptop',1),
('inkjet',2),
('laser',2),
('webserver',3),
('databaseserver',3),
('mailserver',3);

CREATE TABLE Producten(
Naam VARCHAR(100) NOT NULL,
Processoren_Id int,
CONSTRAINT fk_Producten_Processoren FOREIGN KEY (Processoren_Id)
REFERENCES Processoren(Id),
Subcategorieen_Id int,
CONSTRAINT fk_Producten_Subcategorieen FOREIGN KEY(Subcategorieen_Id)
REFERENCES Subcategorieen(Id),
Gewicht VARCHAR(15) NOT NULL,
Merken_Id int,
CONSTRAINT fk_Producten_Merken FOREIGN KEY(Merken_Id)
REFERENCES Merken(Id),
Invoorraad BOOL NOT NULL);

-- maak hier de tabel Producten aan zodat volgende instructies zullen werken
-- een naam bevat maximum 100 karakters en is verplicht
-- de drie verwijzingen (herkenbaar aan de naam) zijn verplicht
-- het gewicht bevat maximum 15 karakters en is verplicht
-- het is verplicht bij te houden of een product in voorraad is of niet

INSERT INTO Producten (Naam, Processoren_Id, Subcategorieen_Id, Gewicht, Merken_Id, InVoorraad)
VALUES
('FireX4500',3,6,'77 kg',5,TRUE),
('Color Laserjet 1600',2,4,'20 kg',2,TRUE),
('XPS M1210',1,2,'10 kg',3,TRUE),
('Netra 1280',1,6,'132 kg',5,FALSE),
('946 (all in one)',2,3,'12 kg',3,FALSE),
('6217 Intellistation A Pro',1,1,'22 kg',1,TRUE),
('Escale PL250',3,6,'50 kg',4,FALSE),
('Veriton 6800',1,1,'12 kg',6,TRUE),
('Aspire L100250',1,1,'9 kg',6,TRUE),
('Laserjet P2015d',2,4,'19 kg',2,FALSE);