USE aptunes;
DROP procedure IF EXISTS DemonstrateHandlerOrder;

DELIMITER $$
USE aptunes$$
CREATE PROCEDURE DemonstrateHandlerOrder ()
BEGIN
	DECLARE randomGetal tinyint DEFAULT 0;
	
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION 
    begin 
    SELECT 'Een algemene fout opgevangen';
    end;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '45002' 
    begin
    select 'State 45002 opgevangen. Geen probleem.';
    end;
   
	SET randomGetal = FLOOR(RAND() * 3)+1;
    IF (randomGetal = 1) THEN
		SIGNAL SQLSTATE '45001';
    ELSEIF (randomGetal = 2) THEN
		SIGNAL SQLSTATE '45002';
    ELSEIF (randomGetal = 3) THEN
		SIGNAL SQLSTATE '45003';
    END IF;
END$$

DELIMITER ;