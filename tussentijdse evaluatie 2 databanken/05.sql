USE tussentijdseevaluatie2;
create table Recensies(
Id int auto_increment Primary Key,
Naam char(100) charset utf8mb4 COLLATE utf8mb4_0900_as_cs NOT NULL,
Link char(2083) charset utf8mb4 COLLATE utf8mb4_0900_as_cs Not NULL,
Cijfer tinyint insigned NOT NULL,
Films_Id int NOT NULL,
Constraint fk_Recensies_Films 
Foreign Key (Films_Id)
REFERENCES Films(id)
);
