USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleaseWithSuccess`;

DELIMITER $$
USE `aptunes`$$
CREATE  PROCEDURE `MockAlbumReleaseWithSuccess`(out success bool)
BEGIN
	DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId INT DEFAULT 0; 
    DECLARE randomBandId INT DEFAULT 0;
    
    Select COUNT(*) Into numberOfAlbums From Albums;
    Select COUNT(*) Into numberOfBands From Bands;
    SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
    SET randomBandId = FLOOR(RAND() * numberOfBands) + 1; 
    IF (randomAlbumId, randomBandId) not in (select * from albumreleases)
    THEN
		INSERT INTO AlbumReleases(Albums_Id, Bands_Id)
        VALUES (randomAlbumId, randomBandId);
        set success = 1;
        else 
        set success = 0;
	END IF;
END$$

DELIMITER ;