USE tussentijdseevaluatie1bdb;
Select coalesce( concat(Leden.Voornaam,' ',Leden.Familienaam), 'is nog nooit door iemand uitgeleend') as "Volledige naam"
FROM Ontleningen
LEFT JOIN Leden on Leden_Id = Leden.Id
Where Leden_Id is null
union all
Select coalesce(Boeken.Titel, 'heeft nog nooit iets uitgeleend')
From Ontleningen
Right JOIN Boeken on Boeken_Id = Boeken.Id
Where Boeken_Id is null;