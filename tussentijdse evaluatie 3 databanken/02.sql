USE tussentijdseevaluatie1bdb;
CREATE view OnpopulaireBoeken
AS
Select Boeken.Titel
From Boeken
LEFT JOIN Ontleningen on Boeken.Id = Boeken_Id
Where Boeken.Id is null