USE ModernWays;
ALTER TABLE huisdieren ADD column Geluid varchar(20) charset utf8mb4;

SET SQL_SAFE_UPDATES = 0;
update huisdieren
set Geluid = 'WAF!'
where Soort = 'hond';
update huisdieren
set geluid = 'miauwww...'
where Soort = 'kat';
SET SQL_SAFE_UPDATES = 1;
