USE tussentijdseevaluatie2;
Select Regisseur ,Genre, avg(Sterren) as 'Gemiddeld aantal sterren' from films
Where Jaar >= 2000
group by Regisseur,Genre 
having avg(Sterren) >= 4 ;