DROP DATABASE IF EXISTS Examen;
CREATE DATABASE Examen;
USE Examen;

CREATE TABLE Merken
(Id INT AUTO_INCREMENT PRIMARY KEY,
 Naam VARCHAR(100) NOT NULL);

INSERT INTO Merken (Naam)
VALUES
('IBM'),
('HP'),
('Dell'),
('Bull'),
('Sun'),
('Acer');

CREATE TABLE Categorieen
(Id INT AUTO_INCREMENT PRIMARY KEY,
 Omschrijving VARCHAR(100) NOT NULL);

INSERT INTO Categorieen (Omschrijving)
VALUES
('pc'),
('printer'),
('server');

CREATE TABLE Processoren
(Id INT AUTO_INCREMENT PRIMARY KEY,
 Omschrijving VARCHAR(100) NOT NULL);

INSERT INTO Processoren (Omschrijving)
VALUES
('3 Ghz'),
('300 Mhz'),
('2,6 Ghz');

CREATE TABLE Subcategorieen
(Id INT AUTO_INCREMENT PRIMARY KEY,
 Omschrijving VARCHAR(100) NOT NULL,
 Categorieen_Id INT NOT NULL,
 CONSTRAINT fk_Subcategorieen_Categorieen FOREIGN KEY (Categorieen_Id) REFERENCES Categorieen(Id));
 
INSERT INTO Subcategorieen (Omschrijving,Categorieen_Id)
VALUES
('desktop',1),
('laptop',1),
('inkjet',2),
('laser',2),
('webserver',3),
('databaseserver',3),
('mailserver',3);

CREATE TABLE Producten
(Id INT AUTO_INCREMENT PRIMARY KEY,
 Naam VARCHAR(100) NOT NULL,
 Processoren_Id INT NOT NULL,
 Subcategorieen_Id INT NOT NULL,
 Gewicht VARCHAR(15) NOT NULL,
 Merken_Id INT NOT NULL,
 InVoorraad BOOL NOT NULL,
 CONSTRAINT fk_Producten_Processoren FOREIGN KEY (Processoren_Id) REFERENCES Processoren(Id),
 CONSTRAINT fk_Producten_Subcategorieen FOREIGN KEY (Subcategorieen_Id) REFERENCES Subcategorieen(Id),
 CONSTRAINT fk_Producten_Merken FOREIGN KEY (Merken_Id) REFERENCES Merken(Id));

INSERT INTO Producten (Naam, Processoren_Id, Subcategorieen_Id, Gewicht, Merken_Id, InVoorraad)
VALUES
('FireX4500',3,6,'77 kg',5,TRUE),
('Color Laserjet 1600',2,4,'20 kg',2,TRUE),
('XPS M1210',1,2,'10 kg',3,TRUE),
('Netra 1280',1,6,'132 kg',5,FALSE),
('946 (all in one)',2,3,'12 kg',3,FALSE),
('6217 Intellistation A Pro',1,1,'22 kg',1,TRUE),
('Escale PL250',3,6,'50 kg',4,FALSE),
('Veriton 6800',1,1,'12 kg',6,TRUE),
('Aspire L100250',1,1,'9 kg',6,TRUE),
('Laserjet P2015d',2,4,'19 kg',2,FALSE);