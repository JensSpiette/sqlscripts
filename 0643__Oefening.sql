use aptunes
DELIMITER $$
CREATE PROCEDURE `CreateAndReleaseAlbum`(
IN titel VARCHAR(100),
IN bands_Id INT
)
BEGIN
start transaction;
insert into albums(Titel)
values (titel);
insert into albumreleases(Bands_Id,Albums_Id)
value (bands_Id,last_insert_id());
commit;
END$$
DELIMITER ;